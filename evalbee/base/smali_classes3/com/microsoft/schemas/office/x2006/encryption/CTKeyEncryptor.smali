.class public interface abstract Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri;
    }
.end annotation


# virtual methods
.method public abstract addNewEncryptedCertificateKey()Lae;
.end method

.method public abstract addNewEncryptedPasswordKey()Lje;
.end method

.method public abstract getEncryptedCertificateKey()Lae;
.end method

.method public abstract getEncryptedPasswordKey()Lje;
.end method

.method public abstract setUri(Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri$Enum;)V
.end method

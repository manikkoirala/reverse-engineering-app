.class public interface abstract Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlToken;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Uri"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri$Enum;
    }
.end annotation


# static fields
.field public static final P0:Lorg/apache/xmlbeans/SchemaType;

.field public static final Q0:Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri$Enum;

.field public static final R0:Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri$Enum;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "uribad9attrtype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri;->P0:Lorg/apache/xmlbeans/SchemaType;

    const-string v0, "http://schemas.microsoft.com/office/2006/keyEncryptor/password"

    invoke-static {v0}, Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri;->Q0:Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri$Enum;

    const-string v0, "http://schemas.microsoft.com/office/2006/keyEncryptor/certificate"

    invoke-static {v0}, Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri;->R0:Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor$Uri$Enum;

    return-void
.end method

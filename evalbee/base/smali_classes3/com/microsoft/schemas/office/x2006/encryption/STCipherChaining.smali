.class public interface abstract Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlToken;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining$Enum;
    }
.end annotation


# static fields
.field public static final S0:Lorg/apache/xmlbeans/SchemaType;

.field public static final T0:Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining$Enum;

.field public static final U0:Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining$Enum;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "stcipherchaining1e98type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining;->S0:Lorg/apache/xmlbeans/SchemaType;

    const-string v0, "ChainingModeCBC"

    invoke-static {v0}, Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining;->T0:Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining$Enum;

    const-string v0, "ChainingModeCFB"

    invoke-static {v0}, Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining;->U0:Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining$Enum;

    return-void
.end method

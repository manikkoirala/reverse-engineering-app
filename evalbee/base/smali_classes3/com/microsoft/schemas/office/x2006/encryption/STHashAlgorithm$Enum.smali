.class public final Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;
.super Lorg/apache/xmlbeans/StringEnumAbstractBase;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Enum"
.end annotation


# static fields
.field static final INT_MD_2:I = 0x7

.field static final INT_MD_4:I = 0x6

.field static final INT_MD_5:I = 0x5

.field static final INT_RIPEMD_128:I = 0x8

.field static final INT_RIPEMD_160:I = 0x9

.field static final INT_SHA_1:I = 0x1

.field static final INT_SHA_256:I = 0x2

.field static final INT_SHA_384:I = 0x3

.field static final INT_SHA_512:I = 0x4

.field static final INT_WHIRLPOOL:I = 0xa

.field private static final serialVersionUID:J = 0x1L

.field public static final table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    new-instance v0, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    new-instance v1, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    const-string v2, "SHA1"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v2, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    const-string v3, "SHA256"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v3, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    const-string v4, "SHA384"

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v4, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    const-string v5, "SHA512"

    const/4 v6, 0x4

    invoke-direct {v4, v5, v6}, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v5, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    const-string v6, "MD5"

    const/4 v7, 0x5

    invoke-direct {v5, v6, v7}, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v6, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    const-string v7, "MD4"

    const/4 v8, 0x6

    invoke-direct {v6, v7, v8}, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v7, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    const-string v8, "MD2"

    const/4 v9, 0x7

    invoke-direct {v7, v8, v9}, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v8, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    const-string v9, "RIPEMD-128"

    const/16 v10, 0x8

    invoke-direct {v8, v9, v10}, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v9, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    const-string v10, "RIPEMD-160"

    const/16 v11, 0x9

    invoke-direct {v9, v10, v11}, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v10, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    const-string v11, "WHIRLPOOL"

    const/16 v12, 0xa

    invoke-direct {v10, v11, v12}, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;-><init>(Ljava/lang/String;I)V

    filled-new-array/range {v1 .. v10}, [Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;-><init>([Lorg/apache/xmlbeans/StringEnumAbstractBase;)V

    sput-object v0, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/apache/xmlbeans/StringEnumAbstractBase;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forInt(I)Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;
    .locals 1

    sget-object v0, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    invoke-virtual {v0, p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;->forInt(I)Lorg/apache/xmlbeans/StringEnumAbstractBase;

    move-result-object p0

    check-cast p0, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    return-object p0
.end method

.method public static forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;
    .locals 1

    sget-object v0, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    invoke-virtual {v0, p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;->forString(Ljava/lang/String;)Lorg/apache/xmlbeans/StringEnumAbstractBase;

    move-result-object p0

    check-cast p0, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    return-object p0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;->forInt(I)Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;

    move-result-object v0

    return-object v0
.end method

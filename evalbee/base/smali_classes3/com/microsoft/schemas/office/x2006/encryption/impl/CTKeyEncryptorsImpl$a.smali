.class public final Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;
.super Ljava/util/AbstractList;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;->getKeyEncryptorList()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public final synthetic a:Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;


# direct methods
.method public constructor <init>(Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;->a:Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;

    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;->a:Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;

    invoke-virtual {v0, p1}, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;->insertNewKeyEncryptor(I)Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;

    move-result-object p1

    invoke-interface {p1, p2}, Lorg/apache/xmlbeans/XmlObject;->set(Lorg/apache/xmlbeans/XmlObject;)Lorg/apache/xmlbeans/XmlObject;

    return-void
.end method

.method public bridge synthetic add(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;->a(ILcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;)V

    return-void
.end method

.method public b(I)Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;->a:Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;

    invoke-virtual {v0, p1}, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;->getKeyEncryptorArray(I)Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;

    move-result-object p1

    return-object p1
.end method

.method public c(I)Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;->a:Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;

    invoke-virtual {v0, p1}, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;->getKeyEncryptorArray(I)Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;->a:Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;

    invoke-virtual {v1, p1}, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;->removeKeyEncryptor(I)V

    return-object v0
.end method

.method public g(ILcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;)Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;->a:Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;

    invoke-virtual {v0, p1}, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;->getKeyEncryptorArray(I)Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;->a:Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;->setKeyEncryptorArray(ILcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;)V

    return-object v0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;->b(I)Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic remove(I)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;->c(I)Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;->g(ILcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;)Lcom/microsoft/schemas/office/x2006/encryption/CTKeyEncryptor;

    move-result-object p1

    return-object p1
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl$a;->a:Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;

    invoke-virtual {v0}, Lcom/microsoft/schemas/office/x2006/encryption/impl/CTKeyEncryptorsImpl;->sizeOfKeyEncryptorArray()I

    move-result v0

    return v0
.end method

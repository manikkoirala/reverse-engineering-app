.class public interface abstract Lcom/microsoft/schemas/office/office/STInsetMode;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlString;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/schemas/office/office/STInsetMode$Enum;
    }
.end annotation


# static fields
.field public static final M0:Lorg/apache/xmlbeans/SchemaType;

.field public static final N0:Lcom/microsoft/schemas/office/office/STInsetMode$Enum;

.field public static final O0:Lcom/microsoft/schemas/office/office/STInsetMode$Enum;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/microsoft/schemas/office/office/STInsetMode;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "stinsetmode3b89type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lcom/microsoft/schemas/office/office/STInsetMode;->M0:Lorg/apache/xmlbeans/SchemaType;

    const-string v0, "auto"

    invoke-static {v0}, Lcom/microsoft/schemas/office/office/STInsetMode$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/office/STInsetMode$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/office/STInsetMode;->N0:Lcom/microsoft/schemas/office/office/STInsetMode$Enum;

    const-string v0, "custom"

    invoke-static {v0}, Lcom/microsoft/schemas/office/office/STInsetMode$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/office/STInsetMode$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/office/STInsetMode;->O0:Lcom/microsoft/schemas/office/office/STInsetMode$Enum;

    return-void
.end method

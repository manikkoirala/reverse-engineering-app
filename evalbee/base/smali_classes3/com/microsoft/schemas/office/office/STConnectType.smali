.class public interface abstract Lcom/microsoft/schemas/office/office/STConnectType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlString;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/schemas/office/office/STConnectType$Enum;
    }
.end annotation


# static fields
.field public static final H0:Lorg/apache/xmlbeans/SchemaType;

.field public static final I0:Lcom/microsoft/schemas/office/office/STConnectType$Enum;

.field public static final J0:Lcom/microsoft/schemas/office/office/STConnectType$Enum;

.field public static final K0:Lcom/microsoft/schemas/office/office/STConnectType$Enum;

.field public static final L0:Lcom/microsoft/schemas/office/office/STConnectType$Enum;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/microsoft/schemas/office/office/STConnectType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "stconnecttype97adtype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lcom/microsoft/schemas/office/office/STConnectType;->H0:Lorg/apache/xmlbeans/SchemaType;

    const-string v0, "none"

    invoke-static {v0}, Lcom/microsoft/schemas/office/office/STConnectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/office/STConnectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/office/STConnectType;->I0:Lcom/microsoft/schemas/office/office/STConnectType$Enum;

    const-string v0, "rect"

    invoke-static {v0}, Lcom/microsoft/schemas/office/office/STConnectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/office/STConnectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/office/STConnectType;->J0:Lcom/microsoft/schemas/office/office/STConnectType$Enum;

    const-string v0, "segments"

    invoke-static {v0}, Lcom/microsoft/schemas/office/office/STConnectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/office/STConnectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/office/STConnectType;->K0:Lcom/microsoft/schemas/office/office/STConnectType$Enum;

    const-string v0, "custom"

    invoke-static {v0}, Lcom/microsoft/schemas/office/office/STConnectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/office/STConnectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/office/STConnectType;->L0:Lcom/microsoft/schemas/office/office/STConnectType$Enum;

    return-void
.end method

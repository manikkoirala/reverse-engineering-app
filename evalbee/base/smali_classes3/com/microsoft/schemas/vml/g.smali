.class public interface abstract Lcom/microsoft/schemas/vml/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/schemas/vml/g$a;
    }
.end annotation


# static fields
.field public static final X0:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/microsoft/schemas/vml/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "ctshapetype5c6ftype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lcom/microsoft/schemas/vml/g;->X0:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewFormulas()Lee;
.end method

.method public abstract addNewHandles()Lfe;
.end method

.method public abstract addNewLock()Lie;
.end method

.method public abstract addNewPath()Lcom/microsoft/schemas/vml/d;
.end method

.method public abstract addNewStroke()Lcom/microsoft/schemas/vml/h;
.end method

.method public abstract addNewTextpath()Lcom/microsoft/schemas/vml/i;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract setAdj(Ljava/lang/String;)V
.end method

.method public abstract setCoordsize(Ljava/lang/String;)V
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setPath2(Ljava/lang/String;)V
.end method

.method public abstract setSpt(F)V
.end method

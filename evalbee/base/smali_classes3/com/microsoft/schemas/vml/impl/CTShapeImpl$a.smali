.class public final Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;
.super Ljava/util/AbstractList;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/schemas/vml/impl/CTShapeImpl;->getClientDataList()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public final synthetic a:Lcom/microsoft/schemas/vml/impl/CTShapeImpl;


# direct methods
.method public constructor <init>(Lcom/microsoft/schemas/vml/impl/CTShapeImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;->a:Lcom/microsoft/schemas/vml/impl/CTShapeImpl;

    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILcom/microsoft/schemas/office/excel/a;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;->a:Lcom/microsoft/schemas/vml/impl/CTShapeImpl;

    invoke-virtual {v0, p1}, Lcom/microsoft/schemas/vml/impl/CTShapeImpl;->insertNewClientData(I)Lcom/microsoft/schemas/office/excel/a;

    move-result-object p1

    invoke-interface {p1, p2}, Lorg/apache/xmlbeans/XmlObject;->set(Lorg/apache/xmlbeans/XmlObject;)Lorg/apache/xmlbeans/XmlObject;

    return-void
.end method

.method public bridge synthetic add(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/microsoft/schemas/office/excel/a;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;->a(ILcom/microsoft/schemas/office/excel/a;)V

    return-void
.end method

.method public b(I)Lcom/microsoft/schemas/office/excel/a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;->a:Lcom/microsoft/schemas/vml/impl/CTShapeImpl;

    invoke-virtual {v0, p1}, Lcom/microsoft/schemas/vml/impl/CTShapeImpl;->getClientDataArray(I)Lcom/microsoft/schemas/office/excel/a;

    move-result-object p1

    return-object p1
.end method

.method public c(I)Lcom/microsoft/schemas/office/excel/a;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;->a:Lcom/microsoft/schemas/vml/impl/CTShapeImpl;

    invoke-virtual {v0, p1}, Lcom/microsoft/schemas/vml/impl/CTShapeImpl;->getClientDataArray(I)Lcom/microsoft/schemas/office/excel/a;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;->a:Lcom/microsoft/schemas/vml/impl/CTShapeImpl;

    invoke-virtual {v1, p1}, Lcom/microsoft/schemas/vml/impl/CTShapeImpl;->removeClientData(I)V

    return-object v0
.end method

.method public g(ILcom/microsoft/schemas/office/excel/a;)Lcom/microsoft/schemas/office/excel/a;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;->a:Lcom/microsoft/schemas/vml/impl/CTShapeImpl;

    invoke-virtual {v0, p1}, Lcom/microsoft/schemas/vml/impl/CTShapeImpl;->getClientDataArray(I)Lcom/microsoft/schemas/office/excel/a;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;->a:Lcom/microsoft/schemas/vml/impl/CTShapeImpl;

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/schemas/vml/impl/CTShapeImpl;->setClientDataArray(ILcom/microsoft/schemas/office/excel/a;)V

    return-object v0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;->b(I)Lcom/microsoft/schemas/office/excel/a;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic remove(I)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;->c(I)Lcom/microsoft/schemas/office/excel/a;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lcom/microsoft/schemas/office/excel/a;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;->g(ILcom/microsoft/schemas/office/excel/a;)Lcom/microsoft/schemas/office/excel/a;

    move-result-object p1

    return-object p1
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/microsoft/schemas/vml/impl/CTShapeImpl$a;->a:Lcom/microsoft/schemas/vml/impl/CTShapeImpl;

    invoke-virtual {v0}, Lcom/microsoft/schemas/vml/impl/CTShapeImpl;->sizeOfClientDataArray()I

    move-result v0

    return v0
.end method

.class public interface abstract Lcom/microsoft/schemas/vml/STStrokeJoinStyle;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlString;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;
    }
.end annotation


# static fields
.field public static final f1:Lorg/apache/xmlbeans/SchemaType;

.field public static final h1:Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;

.field public static final i1:Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;

.field public static final j1:Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/microsoft/schemas/vml/STStrokeJoinStyle;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "ststrokejoinstyle3c13type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lcom/microsoft/schemas/vml/STStrokeJoinStyle;->f1:Lorg/apache/xmlbeans/SchemaType;

    const-string v0, "round"

    invoke-static {v0}, Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/vml/STStrokeJoinStyle;->h1:Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;

    const-string v0, "bevel"

    invoke-static {v0}, Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/vml/STStrokeJoinStyle;->i1:Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;

    const-string v0, "miter"

    invoke-static {v0}, Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/vml/STStrokeJoinStyle;->j1:Lcom/microsoft/schemas/vml/STStrokeJoinStyle$Enum;

    return-void
.end method

.class public Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;
.super Landroid/app/DialogFragment;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;
.implements Landroid/widget/SearchView$OnCloseListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$b;,
        Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;
    }
.end annotation


# instance fields
.field public a:Landroid/widget/ArrayAdapter;

.field public b:Landroid/widget/ListView;

.field public c:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;

.field public d:Landroid/widget/SearchView;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;)Landroid/widget/ArrayAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->a:Landroid/widget/ArrayAdapter;

    return-object p0
.end method

.method public static synthetic b(Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;)Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->c:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;

    return-object p0
.end method

.method public static c(Ljava/util/List;)Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;
    .locals 3

    .line 1
    new-instance v0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

    invoke-direct {v0}, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "items"

    check-cast p0, Ljava/io/Serializable;

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-virtual {v0, v1}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public final d(Landroid/view/View;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "search"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    sget v1, Llb1;->b:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SearchView;

    iput-object v1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->d:Landroid/widget/SearchView;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->d:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->d:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->d:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->d:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->d:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "items"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    sget v1, Llb1;->a:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->b:Landroid/widget/ListView;

    new-instance p1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x1090003

    invoke-direct {p1, v1, v2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->a:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->b:Landroid/widget/ListView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->setTextFilterEnabled(Z)V

    iget-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->b:Landroid/widget/ListView;

    new-instance v0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$a;

    invoke-direct {v0, p0}, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$a;-><init>(Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;)V

    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public e(Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$b;)V
    .locals 0

    .line 1
    return-void
.end method

.method public f(Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->c:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;

    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->f:Ljava/lang/String;

    return-void
.end method

.method public h(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->f:Ljava/lang/String;

    iput-object p2, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->g:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->e:Ljava/lang/String;

    return-void
.end method

.method public onClose()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string v1, "item"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;

    iput-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->c:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;

    :cond_0
    sget p1, Lnb1;->a:I

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->d(Landroid/view/View;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    iget-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->f:Ljava/lang/String;

    if-nez p1, :cond_1

    const-string p1, "CLOSE"

    :cond_1
    iget-object v1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->g:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->e:Ljava/lang/String;

    if-nez p1, :cond_2

    const-string p1, "Select Item"

    :cond_2
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    return-object p1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->b:Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object p1

    check-cast p1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p1}, Landroid/widget/ArrayAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 0

    iget-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->d:Landroid/widget/SearchView;

    invoke-virtual {p1}, Landroid/widget/SearchView;->clearFocus()V

    const/4 p1, 0x1

    return p1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "item"

    iget-object v1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->c:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

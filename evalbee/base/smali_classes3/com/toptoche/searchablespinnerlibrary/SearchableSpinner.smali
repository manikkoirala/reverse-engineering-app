.class public Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;


# static fields
.field public static final NO_ITEM_SELECTED:I = -0x1


# instance fields
.field private _arrayAdapter:Landroid/widget/ArrayAdapter;

.field private _context:Landroid/content/Context;

.field private _isDirty:Z

.field private _isFromInit:Z

.field private _items:Ljava/util/List;

.field private _searchableListDialog:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

.field private _strHintText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 2
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_context:Landroid/content/Context;

    sget-object v0, Lac1;->a:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v1

    sget v2, Lac1;->b:I

    if-ne v1, v2, :cond_0

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_strHintText:Ljava/lang/String;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-direct {p0}, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->init()V

    return-void
.end method

.method private init()V
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_items:Ljava/util/List;

    invoke-static {v0}, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->c(Ljava/util/List;)Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_searchableListDialog:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

    invoke-virtual {v0, p0}, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->f(Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;)V

    invoke-virtual {p0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    iput-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_arrayAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_strHintText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_context:Landroid/content/Context;

    iget-object v2, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_strHintText:Ljava/lang/String;

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const v3, 0x1090003

    invoke-direct {v0, v1, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_isFromInit:Z

    invoke-virtual {p0, v0}, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_0
    return-void
.end method

.method private scanForActivity(Landroid/content/Context;)Landroid/app/Activity;
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    instance-of v1, p1, Landroid/app/Activity;

    if-eqz v1, :cond_1

    check-cast p1, Landroid/app/Activity;

    return-object p1

    :cond_1
    instance-of v1, p1, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_2

    check-cast p1, Landroid/content/ContextWrapper;

    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->scanForActivity(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object p1

    return-object p1

    :cond_2
    return-object v0
.end method


# virtual methods
.method public getSelectedItem()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_strHintText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_isDirty:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_strHintText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_isDirty:Z

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method public onSearchableItemClicked(Ljava/lang/Object;I)V
    .locals 0

    iget-object p2, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_items:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p2

    invoke-virtual {p0, p2}, Landroid/widget/AdapterView;->setSelection(I)V

    iget-boolean p2, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_isDirty:Z

    if-nez p2, :cond_0

    const/4 p2, 0x1

    iput-boolean p2, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_isDirty:Z

    iget-object p2, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_arrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, p2}, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object p2, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_items:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    invoke-virtual {p0, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    :cond_0
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_1

    iget-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_arrayAdapter:Landroid/widget/ArrayAdapter;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_items:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_arrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_items:Ljava/util/List;

    iget-object v1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_arrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_searchableListDialog:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_context:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->scanForActivity(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "TAG"

    invoke-virtual {p1, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return p2
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .line 1
    check-cast p1, Landroid/widget/SpinnerAdapter;

    invoke-virtual {p0, p1}, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/SpinnerAdapter;)V
    .locals 3

    .line 2
    iget-boolean v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_isFromInit:Z

    if-nez v0, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/widget/ArrayAdapter;

    iput-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_arrayAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_strHintText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_isDirty:Z

    if-nez v0, :cond_1

    new-instance p1, Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_context:Landroid/content/Context;

    iget-object v1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_strHintText:Ljava/lang/String;

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    const v2, 0x1090003

    invoke-direct {p1, v0, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_isFromInit:Z

    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public setOnSearchTextChangedListener(Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$b;)V
    .locals 1

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_searchableListDialog:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

    invoke-virtual {v0, p1}, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->e(Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$b;)V

    return-void
.end method

.method public setPositiveButton(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_searchableListDialog:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

    invoke-virtual {v0, p1}, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->g(Ljava/lang/String;)V

    return-void
.end method

.method public setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_searchableListDialog:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

    invoke-virtual {v0, p1, p2}, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->h(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->_searchableListDialog:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

    invoke-virtual {v0, p1}, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->i(Ljava/lang/String;)V

    return-void
.end method

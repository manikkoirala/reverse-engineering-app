.class public Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->d(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;


# direct methods
.method public constructor <init>(Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$a;->a:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    iget-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$a;->a:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

    invoke-static {p1}, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->b(Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;)Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;

    move-result-object p1

    iget-object p2, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$a;->a:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

    invoke-static {p2}, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;->a(Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;)Landroid/widget/ArrayAdapter;

    move-result-object p2

    invoke-virtual {p2, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {p1, p2, p3}, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$SearchableItem;->onSearchableItemClicked(Ljava/lang/Object;I)V

    iget-object p1, p0, Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog$a;->a:Lcom/toptoche/searchablespinnerlibrary/SearchableListDialog;

    invoke-virtual {p1}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

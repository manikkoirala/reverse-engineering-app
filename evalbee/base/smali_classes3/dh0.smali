.class public abstract Ldh0;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lxs1;

.field public static final b:Lxs1;

.field public static final c:Lxs1;

.field public static final d:Lxs1;

.field public static final e:Lxs1;

.field public static final f:Lsw;

.field public static final g:Lsw;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lxs1;

    const-string v1, "COMPLETING_ALREADY"

    invoke-direct {v0, v1}, Lxs1;-><init>(Ljava/lang/String;)V

    sput-object v0, Ldh0;->a:Lxs1;

    new-instance v0, Lxs1;

    const-string v1, "COMPLETING_WAITING_CHILDREN"

    invoke-direct {v0, v1}, Lxs1;-><init>(Ljava/lang/String;)V

    sput-object v0, Ldh0;->b:Lxs1;

    new-instance v0, Lxs1;

    const-string v1, "COMPLETING_RETRY"

    invoke-direct {v0, v1}, Lxs1;-><init>(Ljava/lang/String;)V

    sput-object v0, Ldh0;->c:Lxs1;

    new-instance v0, Lxs1;

    const-string v1, "TOO_LATE_TO_CANCEL"

    invoke-direct {v0, v1}, Lxs1;-><init>(Ljava/lang/String;)V

    sput-object v0, Ldh0;->d:Lxs1;

    new-instance v0, Lxs1;

    const-string v1, "SEALED"

    invoke-direct {v0, v1}, Lxs1;-><init>(Ljava/lang/String;)V

    sput-object v0, Ldh0;->e:Lxs1;

    new-instance v0, Lsw;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lsw;-><init>(Z)V

    sput-object v0, Ldh0;->f:Lsw;

    new-instance v0, Lsw;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lsw;-><init>(Z)V

    sput-object v0, Ldh0;->g:Lsw;

    return-void
.end method

.method public static final synthetic a()Lxs1;
    .locals 1

    .line 1
    sget-object v0, Ldh0;->a:Lxs1;

    return-object v0
.end method

.method public static final synthetic b()Lxs1;
    .locals 1

    .line 1
    sget-object v0, Ldh0;->c:Lxs1;

    return-object v0
.end method

.method public static final synthetic c()Lsw;
    .locals 1

    .line 1
    sget-object v0, Ldh0;->g:Lsw;

    return-object v0
.end method

.method public static final synthetic d()Lsw;
    .locals 1

    .line 1
    sget-object v0, Ldh0;->f:Lsw;

    return-object v0
.end method

.method public static final synthetic e()Lxs1;
    .locals 1

    .line 1
    sget-object v0, Ldh0;->e:Lxs1;

    return-object v0
.end method

.method public static final synthetic f()Lxs1;
    .locals 1

    .line 1
    sget-object v0, Ldh0;->d:Lxs1;

    return-object v0
.end method

.method public static final g(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 1
    instance-of v0, p0, Lqe0;

    if-eqz v0, :cond_0

    new-instance v0, Lre0;

    check-cast p0, Lqe0;

    invoke-direct {v0, p0}, Lre0;-><init>(Lqe0;)V

    move-object p0, v0

    :cond_0
    return-object p0
.end method

.method public static final h(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 1
    instance-of v0, p0, Lre0;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lre0;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, v0, Lre0;->a:Lqe0;

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p0, v0

    :cond_2
    :goto_1
    return-object p0
.end method

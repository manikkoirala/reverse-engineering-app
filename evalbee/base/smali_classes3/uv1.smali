.class public final Luv1;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Luv1;

.field public static final b:Ljava/lang/ThreadLocal;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Luv1;

    invoke-direct {v0}, Luv1;-><init>()V

    sput-object v0, Luv1;->a:Luv1;

    new-instance v0, Lxs1;

    const-string v1, "ThreadLocalEventLoop"

    invoke-direct {v0, v1}, Lxs1;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lwv1;->a(Lxs1;)Ljava/lang/ThreadLocal;

    move-result-object v0

    sput-object v0, Luv1;->b:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lsx;
    .locals 1

    .line 1
    sget-object v0, Luv1;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    return-object v0
.end method

.method public final b()Lsx;
    .locals 2

    .line 1
    sget-object v0, Luv1;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsx;

    if-nez v1, :cond_0

    invoke-static {}, Ltx;->a()Lsx;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    return-object v1
.end method

.method public final c()V
    .locals 2

    .line 1
    sget-object v0, Luv1;->b:Ljava/lang/ThreadLocal;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public final d(Lsx;)V
    .locals 1

    .line 1
    sget-object v0, Luv1;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, p1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    return-void
.end method

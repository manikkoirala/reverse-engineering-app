.class public Lzd;
.super Lh;
.source "SourceFile"


# instance fields
.field public final d:C

.field public final e:C

.field public final f:C


# direct methods
.method public constructor <init>(Ljava/io/Writer;)V
    .locals 1

    .line 1
    const/16 v0, 0x2c

    invoke-direct {p0, p1, v0}, Lzd;-><init>(Ljava/io/Writer;C)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;C)V
    .locals 1

    .line 2
    const/16 v0, 0x22

    invoke-direct {p0, p1, p2, v0}, Lzd;-><init>(Ljava/io/Writer;CC)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;CC)V
    .locals 1

    .line 3
    const/16 v0, 0x22

    invoke-direct {p0, p1, p2, p3, v0}, Lzd;-><init>(Ljava/io/Writer;CCC)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;CCC)V
    .locals 6

    .line 4
    const-string v5, "\n"

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lzd;-><init>(Ljava/io/Writer;CCCLjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;CCCLjava/lang/String;)V
    .locals 0

    .line 5
    invoke-direct {p0, p1, p5}, Lh;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    iput-char p4, p0, Lzd;->f:C

    iput-char p3, p0, Lzd;->e:C

    iput-char p2, p0, Lzd;->d:C

    return-void
.end method


# virtual methods
.method public c([Ljava/lang/String;ZLjava/lang/Appendable;)V
    .locals 4

    .line 1
    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_4

    if-eqz v0, :cond_1

    iget-char v1, p0, Lzd;->d:C

    invoke-interface {p3, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    :cond_1
    aget-object v1, p1, v0

    if-nez v1, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {p0, v1}, Lzd;->h(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, p2, p3, v2}, Lzd;->d(ZLjava/lang/Appendable;Ljava/lang/Boolean;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0, v1, p3}, Lzd;->g(Ljava/lang/String;Ljava/lang/Appendable;)V

    goto :goto_1

    :cond_3
    invoke-interface {p3, v1}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    :goto_1
    invoke-virtual {p0, p2, p3, v2}, Lzd;->d(ZLjava/lang/Appendable;Ljava/lang/Boolean;)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lh;->b:Ljava/lang/String;

    invoke-interface {p3, p1}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    iget-object p1, p0, Lh;->a:Ljava/io/Writer;

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

.method public final d(ZLjava/lang/Appendable;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    if-nez p1, :cond_0

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    iget-char p1, p0, Lzd;->e:C

    if-eqz p1, :cond_1

    invoke-interface {p2, p1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    :cond_1
    return-void
.end method

.method public e(C)Z
    .locals 3

    .line 1
    iget-char v0, p0, Lzd;->e:C

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    if-eq p1, v0, :cond_1

    iget-char v0, p0, Lzd;->f:C

    if-eq p1, v0, :cond_1

    iget-char v0, p0, Lzd;->d:C

    if-eq p1, v0, :cond_1

    const/16 v0, 0xa

    if-ne p1, v0, :cond_2

    goto :goto_0

    :cond_0
    if-eq p1, v0, :cond_1

    iget-char v0, p0, Lzd;->f:C

    if-ne p1, v0, :cond_2

    :cond_1
    :goto_0
    move v1, v2

    :cond_2
    return v1
.end method

.method public f(Ljava/lang/Appendable;C)V
    .locals 1

    .line 1
    iget-char v0, p0, Lzd;->f:C

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lzd;->e(C)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-char v0, p0, Lzd;->f:C

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    :cond_0
    invoke-interface {p1, p2}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    return-void
.end method

.method public g(Ljava/lang/String;Ljava/lang/Appendable;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p0, p2, v1}, Lzd;->f(Ljava/lang/Appendable;C)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public h(Ljava/lang/String;)Z
    .locals 2

    .line 1
    iget-char v0, p0, Lzd;->e:C

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget-char v0, p0, Lzd;->f:C

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    iget-char v0, p0, Lzd;->d:C

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "\r"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.class public abstract Ldk;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lxs1;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lxs1;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1}, Lxs1;-><init>(Ljava/lang/String;)V

    sput-object v0, Ldk;->a:Lxs1;

    return-void
.end method

.method public static final synthetic a()Lxs1;
    .locals 1

    .line 1
    sget-object v0, Ldk;->a:Lxs1;

    return-object v0
.end method

.method public static final b(Lek;)Lek;
    .locals 2

    .line 1
    :cond_0
    :goto_0
    invoke-static {p0}, Lek;->a(Lek;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {}, Ldk;->a()Lxs1;

    move-result-object v1

    if-ne v0, v1, :cond_1

    return-object p0

    :cond_1
    check-cast v0, Lek;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lek;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public static final c(Llk1;JLq90;)Ljava/lang/Object;
    .locals 4

    .line 1
    :cond_0
    :goto_0
    iget-wide v0, p0, Llk1;->c:J

    cmp-long v0, v0, p1

    if-ltz v0, :cond_2

    invoke-virtual {p0}, Llk1;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {p0}, Lmk1;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0

    :cond_2
    :goto_1
    invoke-static {p0}, Lek;->a(Lek;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {}, Ldk;->a()Lxs1;

    move-result-object v1

    if-ne v0, v1, :cond_3

    sget-object p0, Ldk;->a:Lxs1;

    invoke-static {p0}, Lmk1;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0

    :cond_3
    check-cast v0, Lek;

    check-cast v0, Llk1;

    if-eqz v0, :cond_5

    :cond_4
    :goto_2
    move-object p0, v0

    goto :goto_0

    :cond_5
    iget-wide v0, p0, Llk1;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p3, v0, p0}, Lq90;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llk1;

    invoke-virtual {p0, v0}, Lek;->l(Lek;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Llk1;->h()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lek;->k()V

    goto :goto_2
.end method
